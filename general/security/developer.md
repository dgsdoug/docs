# Security Releases (Critical / Non-critical) as a Developer

The release deadlines for a critical or non-critical security release are different.
Check the [Security Release deadlines] first to know when the security fixes have to be merged by.

## Overview

[Delivery team] recorded a video explaining the security release process from an engineering perspective.
Make sure to see it to get an overview of the necessary work.

* [Video on GitLab Unfiltered] - Internal access only

The process can be overwhelming and, to ensure all steps are done correctly and consistently, it's
recommended that you:

* Watch the video listed above
* Follow the steps listed on [process](#process)
* Take a look at the Security release [frequently asked questions](https://about.gitlab.com/handbook/engineering/releases/#frequently-asked-questions)
* Ask questions on the [`#releases`](https://gitlab.slack.com/archives/C0XM5UU6B) Slack channel

After reading and watching the aforementioned material, you may consider reaching out to a teammate to double-check that all the procedures are followed correctly. The process is [continuously improving](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/168), so be sure to check back frequently for updates.

### DO NOT PUSH TO `gitlab-org/gitlab`

As a developer working on a fix for a security vulnerability, your main concern
is not disclosing the vulnerability or the fix before we're ready to publicly
disclose it.

To that end, you'll need to be sure that security vulnerabilities:

* For GitLab.com, are fixed in the [GitLab Security Repo].
* For Omnibus, are fixed in the [Omnibus Security Repo].
* For GitLab Pages, are fixed in the [GitLab Pages Security Repo].
* For Gitaly, are fixed in the [Gitaly Security Repo].

This is fundamental to our security release process because Security repositories are not publicly-accessible.

## Preparation

Before starting, add the new `security` remote on your local GitLab repository:

```sh
git remote add security git@gitlab.com:gitlab-org/security/gitlab.git
```

Then run the `scripts/security-harness` script. This script will install a Git `pre-push` hook
that will prevent pushing to any remote besides `gitlab.com/gitlab-org/security` or `dev.gitlab.org`,
in order to prevent accidental disclosure.

Please make sure the output of running `scripts/security-harness` is:

```sh
Security harness installed -- you will only be able to push to gitlab.com/gitlab-org/security!
```

## Process

As with most GitLab development, a security fix starts with an issue identifying the vulnerability. For GitLab.com,
it should be a confidential issue in the `gitlab-org/gitlab` project or other appropriate canonical repository such
as `gitlab-org/gitaly`, `gitlab-org/gitlab-workhorse`, `gitlab-org/gitlab-runner`, etc.

Typically, only confidential issues that are security vulnerabilities or dependency updates are to follow the process detailed below.
If you are unsure if this process needs to be followed, please [ask the AppSec team](#contact-the-appsec-team). Security vulnerability issues are typically
marked with the `security` label, have a `priority` and `severity` label/score tied to them and do **not** [have the feature label](https://about.gitlab.com/handbook/engineering/security/#feature).
For more information, please see [this section of the handbook](https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues).

**NOTE:** If the fix for the security vulnerability you are working on has a
[far-reaching impact](https://about.gitlab.com/handbook/engineering/development/#reducing-the-impact-of-far-reaching-work),
[contact the AppSec team](#contact-the-appsec-team) and determine if we can reclassify the security issue as a feature change.
If reclassified, this means the fix can be made in the regular `gitlab-org/gitlab` repository instead of through the security process and allow a
[controlled roll out](https://about.gitlab.com/handbook/engineering/development/processes/rollout-plans/).

Once an eligible confidential security issue is assigned to a developer:

1. A [security implementation issue](#security-implementation-issue) must be created on the respective Security repository
   using the security issue template.
   * i.e. If an engineer is working on a security fix for GitLab, they will need to create an issue on [GitLab Security repo]
    using the [GitLab Security issue template]).
   * The security implementation issue describes the required steps that need to be followed to remediate the security
     vulnerability. The same steps are summarized in this section.
   * **IMPORTANT:** Link your security implementation issue to the [next Security Release Tracking issue](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=upcoming%20security%20release) or the security release will not pick up your issue
1. A [security merge request](#security-merge-requests) targeting the default branch (e.g `master` or `main`) must be submitted with the security fix using
   the security merge request template.
   * i.e. If an engineer is working on a security fix for GitLab, they will need to submit the merge request using the
    [GitLab Security merge request template].
   * Merge requests on [GitLab Security] follow the same [code review and approval](#code-reviews-and-approvals) process as any other change.
   * Additionally, the merge request targeting the default branch needs to be approved by an AppSec team member. See the [code reviews and approval](#code-reviews-and-approvals) section for details on who to ping.
1. Once the merge request targeting the default branch is approved according to our Approval guidelines and by an AppSec team member, the
   engineer can proceed to prepare the [backports](#backports)
1. [Backports](#backports) need to be approved by the same maintainer that reviewed and approved the merge request targeting the default branch.
   * It's not required for the backports to have the AppSec approval.
1. Once the merge request targeting the default branch and the backports are ready, they must be assigned to `@gitlab-release-tools-bot`.

### Security implementation issue

Create an issue on the respective repo, [GitLab Security Repo] or [Omnibus Security Repo], using the
[GitLab Security issue template]. The title should be the same as the original created on the Canonical repository,
for example: `Prevent stored XSS in code blocks`.

It's not required to copy all the labels from the original issue on `gitlab-org/gitlab`, ~security and ~severity::x are enough.

This issue is now your "Implementation issue" and a single source of truth for
all related issues and merge requests. Once the issue is created, assign it
to yourself and start working on the tasks listed there.

#### Versions Affected

Within the description "Details" section, fill out the `Versions affected`
using one of the following formats:

* `All`
* `X.Y+`
* `X.Y - X.Y`
* `X.Y,X.Y`

### Security merge requests

[Security implementation issue](#security-implementation-issue) will ask you to create merge requests on the Security repository.
Ensure they are using the respective Security template.

Your branch name must start with `security`, such as: `security-rs-milestone-xss-12-6`.

Branch names that start with `security` cannot be pushed to the canonical repositories on `gitlab.com/gitlab-org`.
This helps ensure security merge requests don't get leaked prematurely.

For the first merge request, make sure:

* Targets the `master` or `main` depending on the repository.
* Has no milestone assigned.
  * This is not needed, and frequently causes confusion because we sometimes start working on a security
    issue during the current milestone, and sometimes at the beginning of the next one.
* Has correct labels (normally ~security and ~severity::x are enough).
* Has green pipelines.

For every backport merge request created, make sure:

* Targets the `X-Y-stable{,-ee}` branch that belongs to the target version.
  * For the MR targeting the current release, the stable branch might not yet exist.
    They are normally created around the 18th. In the meantime, you can set the MR to target the default branch
    instead, and change it later once the stable branch has been created.
    If you're planning to take time off during the time when the stable branch is created,
    you can ask somebody from your team for help and reassign the security issue to them.
* Has a milestone assigned, e.g. a `11-10-stable-` backport MR would have `11.10` set as its milestone.
  * Milestones that have already been closed are not displayed in the UI, but can still be set using the quick action command `/milestone %X.Y`.
* Has correct labels (normally ~security and ~severity::x are enough).
* Has green pipelines.

**IMPORTANT:**

* In case one of these items is missing or incorrect, Release Managers will re-assign
all related merge requests to the original author and remove the issue from the current security release.
This is necessary due to the number of merge requests that need to be handled for each security release.
* Merge requests targeting the default branch are to be merged by Release Managers only.
* [Feature flags are discouraged] from security merge requests.
* Migrations that modify or delete data are discouraged from security merge requests. Best practice would instead be to hide the offending data in the application layer and follow up with a migration using the [regular development process](https://docs.gitlab.com/ee/development/database/).

**TIP:**
When implementing a security fix, it's best to go with the smallest change possible.
This is helpful to avoid problems/conflicts when creating backports for older
versions. It also helps to reduce the possibility of having unwanted side effects
as the fix will be focused on the issue. Improvements can be done publicly after
the security release is done.

### Backports

Because all security fixes go into [at least three monthly releases], three additional branches
targeting the last 3 monthly releases, including the current one, will need to be created for
your security fix.

For example if the work is being done to release 13.4 version, then the backports should
target: `13-4-stable-ee`, `13-3-stable-ee` and `13-2-stable-ee`.

With the backports, the total number of merge requests created for a single security fix is 4.

Follow the guidelines in the [Security Merge Requests](#security-merge-requests) to make sure
the backport merge requests are valid.

**TIP:**
When creating your merge requests backports there's a handy script, [`secpick`](utilities/secpick_script.md),
that will allow you to cherry-pick commits across multiple releases. If changes
can't be cleanly picked (e.g. file changed doesn't exist or file was moved in the
previous version), you will need to fix it manually.

### Code reviews and Approvals

Security merge requests follow the same review process stated in our [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html):

* Merge requests targeting the default branch should be approved:
  * According to our [Approval Guidelines](https://docs.gitlab.com/ee/development/code_review.html#approval-guidelines)
  * By a member of the AppSec team: [contact the AppSec team](#contact-the-appsec-team) for further help.
* Backport merge requests should be approved by at least one maintainer. The Maintainer must be the same as the one assigned to the merge request targeting the default branch. AppSec approval is not needed for backport merge requests.

### Final steps

* Ensure all items have been completed:
  * Including [Summary section] from the security implementation issue.
  * Items listed on the [GitLab Security merge request template] for each merge request.
* Ensure all merge requests associated to the security implementation issue are assigned to `@gitlab-release-tools-bot`,
ping the corresponding maintainer if that's not the case.
* Be sure to run `scripts/security-harness` again to enable pushing to remotes other than [GitLab Security].

## Contact the AppSec team

To contact the AppSec team, please `@-mention` the [AppSec stable counterpart](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/stable-counterparts.html) for the [group relevant to the vulnerability being fixed](https://about.gitlab.com/handbook/product/product-categories/#devops-stages). Alternatively, ping the AppSec engineer associated to the issue in the [Canonical repository](https://gitlab.com/gitlab-org/gitlab) or ask in the `#sec-appsec` Slack channel.

## Questions?

If you have any doubts or questions, feel free to ask for help in the #releases or #g_delivery
channel in Slack.

---

[Security Release deadlines]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md#security-release-deadlines
[at least three monthly releases]: https://docs.gitlab.com/ee/policy/maintenance.html#security-releases
[GitLab.com]: https://gitlab.com/
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[Delivery team]: https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/
[Video on GitLab Unfiltered]: https://www.youtube.com/watch?v=ixtUDxM3nWA

[GitLab Security]: https://gitlab.com/gitlab-org/security/
[GitLab Security repo]: https://gitlab.com/gitlab-org/security/gitlab
[Omnibus Security repo]: https://gitlab.com/gitlab-org/security/omnibus-gitlab
[Gitaly Security repo]: https://gitlab.com/gitlab-org/security/gitaly
[GitLab Pages Security Repo]: https://gitlab.com/gitlab-org/security/gitlab-pages

[GitLab Security issue template]: https://gitlab.com/gitlab-org/security/gitlab/issues/new?issuable_template=Security+developer+workflow
[GitLab Security merge request template]: https://gitlab.com/gitlab-org/gitlab/blob/master/.gitlab/merge_request_templates/Security%20Release.md

[scripts/security-harness]: https://gitlab.com/gitlab-org/gitlab/blob/master/scripts/security-harness
[Summary section]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md#summary
[Feature flags are discouraged]: ./utilities/feature_flags.md
