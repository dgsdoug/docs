# Release Manager

The release manager drives the [monthly release] of GitLab, any
[patch releases] and [security releases] for that version.

To find the current release managers, please consult the [release manager schedule].

## Responsibilities

You as a release manager have a responsibility to deliver the work of every
single person involved in creating the product and running the application at
GitLab.com.

This responsibility sometimes requires making difficult decisions. For example,
you might need to refuse including a feature or a change within a release.
You might need to decide on shipping a feature that is not working as expected
to allow for other features/fixes to be available. You might need to revert
someone's work because it was impacting the release schedule.

The decisions you make can have a cascading effect on all other release tasks,
so make sure that you collect as much data as you can within the time you have,
make an informed decision, and stick with it.

At all times, keep in mind that protecting GitLab users and the stability of
GitLab.com are more important than accepting a change in release to please
someone from you or another team.

When you start as a release manager, some of your responsibilities are:

* Escalating to responsible parties in case a release task is slowed down or
  blocked by their area of responsibility
* Ensuring that the automated QA is successfully completed
* Monitoring our auto-deploy process
* Deploying to Production
* Understanding the impact of the deployment on environment
* Communicating with the public through writing release blog posts
* [Handling deployment failures](../general/deploy/failures.md)
* Acting as DRI for "Near Miss" incidents relating to deployments


The monthly self-managed releases are a company-wide effort, and should not
fall entirely on the release manager's shoulders. More about that in the sections
that follow.

Just one final thing before you get started: keep calm and move fast!

## Getting Started

If you are assigned to the release management duty, the first course of action
should be:

1. To [onboard](#onboarding) and,
2. To create a [monthly release issue].

With access to tools required to operate the releases, you can start with your
first task.

### Onboarding

Create a [new issue in the **release/tasks** project][new issue] and select the
`RM-Onboarding` template. Use the title `Onboarding Release Manager YOUR_NAME_HERE`.

Assign the issue to the [previous Release Manager][managers] in your timezone.

The tasks are ordered by priority and should be completed before starting your
appointed release.

### Permissions

To fullfil their responsibilities, Release Managers require push and merge access on protected branches
in the following repositories:

* GitLab
* GitLab FOSS
* Omnibus-GitLab
* Gitaly
* GitLab Chart
* GitLab Agent
* GitLab Pages
* GitLab Workhorse
* CNG
* GitLab Elasticsearch Indexer

Permissions are granted through the  `@gitlab-org/release/managers` group which Release Managers
are added automatically when their shift starts.

`@gitlab-org/release/managers` group has the following permissions:

**For [Canonical](https://gitlab.com/gitlab-org) and [Security](https://gitlab.com/gitlab-org/security/) repositories**

Release Managers:

* Have `maintainer` access.
* Are allowed to `push` and `merge` to `*-auto-deploy-*` and `*-stable` branches.
* Are allowed to `push` to the default branch (e.g `master` or `main`).

**For [Dev](dev.gitlab.org) repositories**

Dev is a CE instance, therefore the 'Protected branch' feature is limited to role.
In this case, ~"team::Delivery" members have maintainer access to all the projects.

### Bot permissions

`@gitlab-bot` and `@gitlab-release-tools-bot` have maintainer role in all the projects
mentioned above. This is necessary since they perform release operations such as mirroring, 
Gitaly updates, merging security merge requests, among others. They are specifically allowed to `push`
and `merge` on all protected branches.

### Training

Now is a good time to talk with the previous Release Managers. They should
be able to answer any question you have. If they don't know the answer, they should
direct you to a person who might know. Feel free to go as far as necessary to
get your answer. Do remember to document when you find that answer!

### Release Candidates

[Release Candidate (RC)][release-candidates] is a point in time snapshot of what will become a release.
Any RC that gets created can be considered for final release.

### Deployment

The release manager is also responsible for promoting the latest versions through GitLab.com environments.

### QA task

Quality assurance (QA) is how we reduce the possibility of shipping a broken
feature. We have two types of QA, automated and manual.

Automated QA is ran with deployment pipelines.
Manual QA is in a form of an issue with a list of MRs that engineers
need to review through.

Your responsibility is to monitor the automated QA for successful runs
and the issue in case something is reported there. As engineers check off items
they may raise a concern in this issue and together with them it is necessary to
assign a correct priority.

Issues that could cause an incident if deployed should be raised as [Deployment blockers](https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers) and owned by the Release Manager.  

Find out more about QA task in a separate [qa documentation][qa].

### Post-deployment Patch

Post-deployment patches are something we manage the lifecycle of.  It would be
wise to read up and get to know the process as outlined in [the documentation](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/post-deployment-patches.md)

### Patch Release

As the final self-managed release gets published to users on the 22nd, it is
expected to have a number of patch releases to resolve any outstanding bugs
reported after the release.

The amount and scheduling of [patch releases][patch] is entirely at your discretion as
the release manager.

Exception is reserved for [critical security releases](../general/security/process.md#critical-security-releases), which should be
addressed immediately. [Security process](../general/security/process.md)
diverges from this process.

If a bug affects a large number of users and/or a critical piece of
functionality, it's fine to release a patch with only one fix. Sometimes a patch
will include five or more minor fixes. You should use your
best judgment to determine when a patch release is warranted. If you are not sure,
you can always ask for help on deciding at #releases. We strive to
continue releasing patches until all (or most) known regressions for that release are
addressed.

To help you understand what is expected from you when doing patch releases,
check out [Patch release documentation][patch].

## Guidelines for Release Issue Assignment

The Delivery team holds weekly status meetings where we decide which members of
the team will become Release Managers for upcoming releases.  We assign Release
Managers to our monthly releases and an [MR is
created](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/release_managers.yml)
to update the [release manager schedule] appropriately.  Monthly Release Issues,
when created, will automatically be assigned to the correct Release Manager
based on the data stored in the [release manager schedule].

### Auto-Deploy Transition

In between monthly releases, auto-deploy continues to operate and this creates a
transition window where we shift between the existing RM's to the new RM's. This
window is loosely defined and should be agreed upon by the two sets of RM's. The
transition can be considered complete when the next monthly release issue is
created.

### Patch Release Assignment

Patches are unique as there are many varieties of them. A guideline to follow is
this:

* RM's assigned to X.1.0 will perform non critical patch releases until X.2.0
  prep work begins
  * This style of patch release is normally unrelated to the upcoming X.2.0
    release.
  * This patch is usually not impacting auto-deploy which is the upcoming RM's
    primary responsibility.
* If a critical security issue is impacting auto-deploy leading up to the X.2.0
  release, the RM's associated with X.2.0 should work on that patch.
  * This is due to the nature of what is required to maintain the
    auto-deployment procedure
  * Since backports of the security patch will most likely be very similar,
    logistically, it's easier for only one set of RM's to be working on a
    release.
* Avoid situations where a group of patch releases that are to be released at
  the same time, are worked on by differing sets of RM's
  * This can create problems with communications for all Engineers involved

### Release Issue Creation

Because release tooling looks at the [release manager schedule] for issue
assignment, issues may be automatically assigned to previous release managers.
It is standard practice to assign created release issues to the desired Release
Manager if it differs.

### Deployment blockers

For any issues that block our ability to deploy, such as high severity bugs, merge conflicts, or failing migrations we should follow the steps in [Deployment blockers](https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers). Being unable to deploy limits our ability to respond to a S1 incident/security issue and so we must move fast to unblock deployments. 

Release Managers will often be the [Owner](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#ownership) of the incident. 

## Offboarding

Once there's no immediate action to create a patch release, Release Managers
duties have finished. To recover from the pressure of release management it is strongly encouraged that you take a week of PTO following your rotation.

[monthly release]: ../general/monthly.md
[patch releases]: ../general/patch/process.md
[security releases]: ../security/process.md
[release-candidates]: ../general/release-candidates.md
[deployment]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab
[qa]: ../general/qa-checklist.md
[patch]: ../general/patch/process.md
[new issue]: https://gitlab.com/gitlab-org/release/tasks/issues/new
[managers]: https://about.gitlab.com/release-managers/
[monthly release issue]: ../general/monthly/release-manager.md#create-an-issue-to-track-the-release
[release manager schedule]: https://about.gitlab.com/community/release-managers/

---

[Return to Quick Start](../README.md#quick-start)
